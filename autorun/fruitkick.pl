use strict;
use Irssi;
use vars qw($VERSION %IRSSI);

$VERSION = "1.02";
%IRSSI = (
	authors		=> "mr_flea SNAPE",
	contact		=> "mrflea\@gmail.com",
	name		=> "fruitkick.pl",
	description	=> "Uses a random fruit name as kick message. EXPANDED BY SNAPE",
	license		=> "BSD",
	url			=> "http://www.phantomflame.com/",
	changed		=> "2012",
);

my @fruits = (
	'papaya',
	'banana',
	'apple',
	'tomato',
	'cranberry',
	'blueberry',
	'raspberry',
	'elderberry',
	'avocado',
	'gooseberry',
	'watermelon',
	'melon',
	'date',
	'boysenberry',
	'blackberry',
	'strawberry',
	'cherry',
	'orange',
	'lemon',
	'lime',
	'grape',
	'kiwi',
	'pomegranate',
	'guava',
	'grapefruit',
	'pineapple',
	'fig',
	'plum',
	'nectarine',
	'mango',
	'cantaloupe',
	'honeydew',
	'durian',
	'lychee',
	'pear',
	'tutti frutti',
	'abiu',
	'chestnut',
	'hazelnut',
	'apricot',
	'tangerine',
	'clementine',
);

sub cmd_fruitkick {
	my ($data, $server, $witem) = @_;

	my $kickee = (split(/ /, $data, 2))[0];

	$witem->command("KICK $kickee " . $fruits[rand $#fruits]);
}

Irssi::command_bind('fruitkick', 'cmd_fruitkick');
