
use strict;
use Irssi;
use vars qw($VERSION %IRSSI);

$VERSION = "0.3";

%IRSSI = (
        authors         => 'c|p',
        contact         => 'cp@eatddos.info',
        name            => 'random_attributes.pl',
        description     => 'Applies random control codes (bold, underline, reverse, color) to text',
        license         => 'WTFPL',
        url             => 'http://irssi.wtfk.us/'
);

my @attribute_codes = ("\x02", "\x03", "\x0f", "\x16", "\x1f");
my @chans = Irssi::settings_get_str('rat_chans');

sub mod_chans
{
        my @args = split(/\s/, $_[0]);
        if ($args[0] =~ "add")
        {
                push(@chans, $args[1]);
        } elsif ($args[0] =~ "del")
        {
                @chans = grep { $_ ne $args[1] } @chans;
        }
        Irssi::settings_set_str('rat_chans', "@chans");
}

sub apply_attributes
{
        my $randomized = '';
        my $original = shift;
        my $i = 0;
        for (; $i < length($original); $i++)
        {
                my $code = $attribute_codes[rand @attribute_codes];
                $randomized .= $code;
                if ($code eq "\x03")
                {
                        $randomized .= sprintf("%02d", int(rand(16)));
                        if (int(rand(2)))
                        {
                                $randomized .= sprintf(",%02d",
int(rand(16)));
                        }
                }
                $randomized .= substr($original, $i, 1);
        }
        return $randomized;
}

sub cmd_rat
{
        my ($args, $server, $witem) = @_;
        my $channel = Irssi::active_win()->get_active_name();
        my $text = apply_attributes($args);
        $server->command("msg $channel $text");
}

sub ot
{
        my ($args, $server, $witem) = @_;
        my $channel = Irssi::active_win()->get_active_name();

        if (Irssi::settings_get_bool('rat_enabled') &&
grep(/\Q$channel\E/i, @chans)) {
                Irssi::signal_stop();
                my $text = apply_attributes($args);
                Irssi::signal_continue($text, $server, $witem);
        }
}

Irssi::settings_add_bool('rat', 'rat_enabled', 0);
Irssi::settings_add_str('rat', 'rat_chans', '');
Irssi::signal_add_first('send text', \&ot);
Irssi::command_bind('rat_mod', 'mod_chans');
Irssi::command_bind('rat_list', sub { print "@chans"; });
Irssi::command_bind('rat', 'cmd_rat');
